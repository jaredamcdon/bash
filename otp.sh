#!/bin/bash

while getopts ":p:k:c:" option
do
    case "${option}"
        in
        p)plain=${OPTARG};;
        k)key=${OPTARG};;
        c)cipher=${OPTARG};;
    esac
done

declare -A alphabet

alphabet=( [A]=0 [B]=1 [C]=2 [D]=3 [E]=4 [F]=5 [G]=6 [H]=7 [I]=8 [J]=9 [K]=10 [L]=11 [M]=12 [N]=13 [O]=14 [P]=15 [Q]=16 [R]=17 [S]=18 [T]=19 [U]=20 [V]=21 [W]=22 [X]=23 [Y]=24 [Z]=25 [" "]=26 )
revAlphabet=( A B C D E F G H I J K L M N O P Q R S T U V W X Y Z " " )

function encrypt_fn {
	#plain="This is a plaintext message"

	plain=${plain^^}

	len=${#plain}

	keyNumeric=()

	for ((i = 0 ; i < $len ; i++))
	do
		randint=$(expr $RANDOM % 27)
		keyNumeric+=($randint)
	done

	plainNumeric=()
	for ((i = 0; i < $len ; i++))
	do
		current=${plain:$i:1}
		plainNumeric+=(${alphabet[$current]})
	done

	cipherNumeric=()
	for ((i = 0; i < $len ; i++))
	do
		cipherNum=$(expr ${plainNumeric[$i]} + ${keyNumeric[$i]})
		cipherNumeric+=($(expr $cipherNum % 27))

	done

	cipher=""
	key=""
	for ((i = 0; i < $len ; i++))
	do
		cipher+=${revAlphabet[${cipherNumeric[$i]}]}
		key+=${revAlphabet[${keyNumeric[$i]}]}
	done
}

function decrypt_fn {
    #cipher="JQOJGIPVDUOFVFUECVCIWHFQRZV"
    #key="RJGSHAYWDV VVYHMZZKJKDOZRTR"
    len=${#cipher}

    keyNumeric=()
    cipherNumeric=()
    for ((i = 0 ; i < $len ; i++))
    do
        current=${cipher:$i:1}
        cipherNumeric+=(${alphabet[$current]})
        current=${key:$i:1}
        keyNumeric+=(${alphabet[$current]})
    done

	decrypt=""
	for ((i = 0; i < $len ; i++))
	do
		decryptNum=$(expr ${cipherNumeric[$i]} - ${keyNumeric[$i]})
		if [ $decryptNum -lt 0 ]
		then
			decrypt+=${revAlphabet[$(expr $decryptNum + 27)]}
		else
			decrypt+=${revAlphabet[$(expr $decryptNum % 27)]}
		fi
	done
}

#this is breaking when key and cipher given
if [ -n "$plain" ]
then
    encrypt_fn
elif [ -n "$cipher" ]
then
    if [ -n "$key" ]
    then
        decrypt_fn
    else
        echo "please provide a key"
    fi
elif [ -n "$key" ]
then
    if [ -n "$cipher"]
    then
        decrypt_fn
    else
        echo "please provide a cipher"
    fi
else
    echo "No parameters provided; defaulting."
    plain="This is a plaintext message"
    encrypt_fn
    decrypt_fn
fi

echo ${plainNumeric[@]}
echo ${keyNumeric[@]}
echo ${cipherNumeric[@]}
echo $plain
echo $key
echo $cipher
echo $decrypt