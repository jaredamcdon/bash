#!/usr/bin/env bash

result=$(curl ifconfig.me)

ARIN=(3, 4, 8, 9, 13, 15, 16, 18, 20, 23, 24, 32, 34, 40, 44, 45, 47, 50, 52, 54, 56, 63, 64, 65, 66, 
	67, 68, 69, 70, 71, 72, 74, 75, 76, 96, 97, 98, 99, 100, 104, 107, 108, 128, 129, 130, 131, 
	132, 134, 135, 136, 137, 138, 139, 140, 142, 143, 144, 146, 147, 148, 149, 152, 155, 156, 
	157, 158, 159, 160, 161, 162, 164, 165, 166, 167, 168, 169, 172, 173, 174, 184, 192, 198, 
	199, 204, 205, 206, 207, 208, 209, 216)

while IFS="." read -ra ADDR; do
	block1=${ADDR[0]}
done <<< "$result"

echo $block1


if [[ "${ARIN[*]}" =~ "$block1" ]]; then
	echo "IP in ARIN range! System powered off! $(date)" > ~/cronlog
	# add additional commands here as needed
	poweroff
else
	echo $result > ~/cronStatus
fi
