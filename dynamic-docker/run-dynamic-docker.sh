#!/usr/bin/env bash

#build the container
docker build -t testpy .

# run the container - can be done over and over again
docker run testpy
