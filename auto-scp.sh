file=""
user=""
server=""
dir=""

len=0
for file in *
do
        if [ $(expr $len % 2) == "0" ]
        then
                echo "scping: $file"
                filesToRemove+=("$file")
                sshpass -p $password scp $file $user@$server:$dir
                echo "scp complete: $file"
                result=$(sshpass -p $password ssh $user@$server "sh -c 'if test -f $dir/$file ; then echo "yes" ; fi'")
                if [ "$result" != "yes" ]
                then
                        echo "error occured on file: $file"
                else
                        echo "scp validated for $file"
                        rm $file
                fi

        fi
        len=$((len+1))
        echo $len
done
