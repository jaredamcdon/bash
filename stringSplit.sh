version="3.14.2"

IFS="." read -ra subSection<<<"$version"

version=$(echo "${subSection[0]}.${subSection[1]}")

echo $version

IFS=" "
