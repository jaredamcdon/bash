# echo a header and redirect to new file idle_time.txt
echo -e "username\t|\tidle time" > idle_time.txt
# w displays more granular who ouput, -h removes header
# pipe into 'grep pts' finds all users as pts is in each user's string (for tty)
w -h | grep pts |
# pipe into IFS to read line by line as $line
while IFS= read -r line
do
    # convert to array to split each word into separate string
    arr=($line)
    #find length of each name
    len=$(echo ${#arr[0]})
        #check length to clean output with tabular spacing
    if [ $len -le 4 ]
    then
        # append redirect echo of arr[0] (username) and arr[4] (idle time)
        echo -e ${arr[0]} "\t\t|\t"  ${arr[4]}>> idle_time.txt
    else
            # same as about with one less tab for longer usernames
        echo -e ${arr[0]} "\t|\t"  ${arr[4]}>> idle_time.txt
    fi
done
