#check for first parameter
if [ -z "$1" ]
then
    echo "enter role_arn tail"
    read passed_var
else
    passed_var=$1
fi

#check for second var
if [ -z "$2" ]
then
    echo "output file defaulted to ssm.config"
    outputfile="ssm.config"
else
    outputfile=$2
fi

filename="aws.config"

# reads file line by line, passes line in as $line
while IFS= read -r line
do
    # search for arn in line string
    if [[ $line = *arn* ]]
    then
        # just a bunch of sed separated by ?
        new_line=$(echo $line | sed "s?/.*?/$passed_var?g")
        echo $new_line >>$outputfile
    else
        echo $line >>$outputfile
    fi

# close IFS instance
done < $filename
