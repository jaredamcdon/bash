
declare -a nameList

while IFS= read -r line; do
  nameList+=("$line")
done < $1


# test "$test" = *Content*

for i in "${nameList[@]}"
do
	var1=$(echo $i | grep plain)
	var2=$(echo "$var1" | grep Length)
	if [ ${#var2} -gt 0 ]
	then
		echo "yes"
		echo $var2 >> validated.txt
	fi
done
