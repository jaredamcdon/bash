version=$(sed 's/.*">//g' <<< $(curl https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/?tab=downloads | grep "Full.zip" | head -1))

version=$(sed 's|</a>||g' <<< $version)

link="https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/$version"

wget $link -O kzt.zip
unzip kzt.zip -d ./kzt
rm kzt.zip
